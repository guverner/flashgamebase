﻿package  
{
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class CStage extends Sprite 
	{
		private var m_stageName:String;
		
		public function CStage(stageName:String) 
		{
			super();
			
			m_stageName = stageName;
		}
		
		public function get stageName():String 
		{
			return m_stageName;
		}
		
		public function get touchable():Boolean
		{
			return this.mouseEnabled;
		}
		
		public function set touchable(newValue:Boolean):void
		{
			this.mouseEnabled = newValue;
		}
	}
}