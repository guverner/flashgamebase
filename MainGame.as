﻿package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class MainGame extends Sprite 
	{	
		private static var m_instance:MainGame;
		private static var m_isAllowInstantiation:Boolean = false;
		
		public function MainGame() 
		{
			super();
			
			if (this.stage)
			{
				this.initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		static public function get instance():MainGame 
		{
			if(!m_instance)
			{
				m_instance = new MainGame();
			}
			
			return m_instance;
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		private function initialise():void
		{
			StageManager.instance.pushStage(new MenuStage());
		}
	}
}