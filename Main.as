﻿package 
{
	import flash.events.Event;
	import flash.display.Sprite;

	public class Main extends Sprite
	{
		public function Main()
		{
			super();
			
			if(this.stage)
			{
				initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function initialise():void
		{
			this.addChild(MainGame.instance);
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
	}
}