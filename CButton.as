﻿package
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;
	import flash.display.Sprite;

	public class CButton extends Sprite
	{
		private var m_callback:Function;
		private var m_buttonContainer:Sprite;
		private var m_texture:Bitmap;
		private var m_label:TextField;
		private var m_isMouseOver:Boolean;
		private static const SCALE_NORMAL:Number = 1;
		private static const SCALE_OVER:Number = 1.3;
		private static const SCALE_DOWN:Number = 1.15;
		
		public function CButton(texture:Class, callback:Function, text:String = null, textSize:int = 16)
		{
			super();
			
			m_buttonContainer = new Sprite();
			this.addChild(m_buttonContainer);
			
			m_callback = callback;
			m_isMouseOver = false;
			this.buttonMode = true;
			
			m_texture = new texture() as Bitmap;
			m_buttonContainer.addChild(m_texture);
			
			if(text)
			{
				var textFormat:TextFormat = new TextFormat("RememberFont", textSize);
				textFormat.align = TextFormatAlign.CENTER;
				
				m_label = new TextField();
				//m_label.border = true;
				m_label.y = 8;
				m_label.selectable = false;
				m_label.mouseEnabled = false;
				m_label.defaultTextFormat = textFormat;
				m_label.embedFonts = true;
				m_label.text = text;
				m_label.width = this.width;
				m_label.height = this.height - 10;
				m_buttonContainer.addChild(m_label);
			}
			
			m_buttonContainer.x -= m_buttonContainer.width / 2;
			m_buttonContainer.y -= m_buttonContainer.height / 2;
			
			this.addEventListener(MouseEvent.MOUSE_UP, onButtonUp);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onButtonDown);
			this.addEventListener(MouseEvent.MOUSE_OVER, onButtonOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onButtonOut);
			
			if (this.stage)
			{
				initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onButtonUp(e:MouseEvent):void
		{
			//trace("onButtonUp");
			
			if(m_isMouseOver)
			{
				this.scaleX = this.scaleY = SCALE_OVER;
				m_callback();
			}
			else
			{
				this.scaleX = this.scaleY = SCALE_NORMAL;
			}
		}
		
		private function onButtonDown(e:MouseEvent):void
		{
			//trace("onButtonDown");
			this.scaleX = this.scaleY = SCALE_DOWN;
		}
		
		private function onButtonOver(e:MouseEvent):void
		{
			//trace("onButtonOver");
			m_isMouseOver = true;
			this.scaleX = this.scaleY = SCALE_OVER;
		}
		
		private function onButtonOut(e:MouseEvent):void
		{
			//trace("onButtonOut");
			m_isMouseOver = false;
			this.scaleX = this.scaleY = SCALE_NORMAL;
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		private function initialise():void
		{
			
		}
	}
}
