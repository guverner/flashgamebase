﻿package 
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import flash.display.BitmapData;
	import flash.ui.Keyboard;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class GameStage extends CStage 
	{	
		private static var m_instance:GameStage;
		private static var m_allowInstantiation:Boolean;
		
		private var m_pauseButton:CButton;
		private var m_isPaused:Boolean;
		private var m_layers:Object;
		private static const LAYER_BACKGROUND:String = 	"background";
		private static const LAYER_GAME:String = 		"game";
		private static const LAYER_HUD:String = 		"hud";
		private var m_timers:Array;
		private var m_speed:Number;
		public static var m_timeTotal:Number;
		
		public function GameStage() 
		{
			super(Constants.GAME);
			
			if (!m_allowInstantiation)
			{
				throw new Error("Error: Instantiation failed: Use GameStage.instance instead of new.");
			}
			else
			{
				if (this.stage)
				{
					initialise();
				}
				else
				{
					this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
				}
			}
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		public static function get instance():GameStage
		{
			if (!m_instance) {
				m_allowInstantiation = true;
				m_instance = new GameStage();
				m_allowInstantiation = false;
			}
			
			return m_instance;
		}
		
		public function remove():void
		{
			removeAllLayers();
			m_instance = null;
		}
		
		private function initialise():void
		{
			m_layers = initLayers();
			m_timers = new Array();
			
			createBackground();
			
			m_pauseButton = new CButton(Resources.PauseButtonImage, onPauseButtonClick);
			m_pauseButton.x = m_pauseButton.width / 2 + 8;
			m_pauseButton.y = m_pauseButton.height / 2 + 8;
			getLayer(LAYER_HUD).addChild(m_pauseButton);
			
			m_isPaused = false;
			
			//this.addEventListener(Event.ENTER_FRAME, update);
//			this.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyPressed);
//			this.stage.addEventListener(KeyboardEvent.KEY_UP, onKeyReleased);
		}
		
		private function createBackground():void
		{
			var bmpData:BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight, false, 0x4C4C4C);
			var bitmap:Bitmap = new Bitmap(bmpData);
			getLayer(LAYER_BACKGROUND).addChild(bitmap);
		}
		
		public function get timers():Array
		{
			return m_timers;
		}
		
		private function removeAllLayers():void
		{
			if (!m_layers)
			{
				return;
			}
			
			for (var key:String in m_layers)
			{
				this.removeChild((m_layers[key] as Sprite));
			}
			
			m_layers = null;
		}
		
		private function getLayer(layerName:String):Sprite
		{
			if (m_layers.hasOwnProperty(layerName))
			{
				return (m_layers[layerName] as Sprite);
			}
			
			return null;
		}
		
		private function initLayers():Object
		{
			var layers:Object = new Object();
			
			layers[LAYER_BACKGROUND] = new Sprite();
			this.addChild(layers[LAYER_BACKGROUND]);
			layers[LAYER_GAME] = new Sprite();
			this.addChild(layers[LAYER_GAME]);
			layers[LAYER_HUD] = new Sprite();
			this.addChild(layers[LAYER_HUD]);
			
			return layers;
		}
		
		private function onKeyPressed(e:KeyboardEvent):void
		{
		}
		
		private function onKeyReleased(e:KeyboardEvent):void
		{
		}
		
		private function update(e:Event):void
		{
			if (m_isPaused)
			{
				return;
			}
			
			//var deltaTime:Number = e.passedTime;
//			
//			m_timeTotal += deltaTime;
//			
//			updateTimers(deltaTime);
//			
//			processKeys(deltaTime);
		}
		
		private function updateTimers(dt:Number):void
		{
			var numTimers:uint = m_timers.length;
			for (var indTimer:int = 0; indTimer < numTimers; indTimer++)
			{
				(m_timers[indTimer] as CTimer).update(dt);
			}
		}
		
		
		
		private function processKeys(deltaTime:Number):void
		{
		}
		
		private function onPauseButtonClick():void
		{
			trace("onPauseButtonClick");
			m_isPaused = true;
			StageManager.instance.pushStage(new PauseStage());
		}
		
		public function continueGame():void
		{
			trace("continueGame");
			m_isPaused = false;
		}
		
//		public function restartGame():void
//		{
//			m_isPaused = false;
//			m_speed = START_SPEED;
//			m_timers = null;
//			m_timers = new Array();
//			var timerCountDistance:CTimer = new CTimer(0, 1, onCountDistance);
//			m_isLeftPressed = false;
//			m_isRightPressed = false;
//			m_isSpacePressed = false;
//			removeAllObstacles();
//			m_obstaclesObjects = new Array();
//			m_energyBar.restart();
//			m_hero.score = 0;
//			updateDistanceLabel();
//			m_obstaclesGenerator.restart();
//			trace("restartGame");
//		}
	}
}