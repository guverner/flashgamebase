﻿package 
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.display.Bitmap;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class PauseStage extends CStage 
	{
		private var m_menuContainer:Sprite;
		private var m_pauseLabel:TextField;
		private var m_continueButton:CButton;
		private var m_menuButton:CButton;
		private var m_menuBase:Bitmap;
		
		public function PauseStage() 
		{
			super(Constants.PAUSE);
			
			if (this.stage)
			{
				initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		private function initialise():void
		{
			m_menuContainer = new Sprite();
			m_menuContainer.x = stage.stageWidth / 2 - 150;
			m_menuContainer.y = stage.stageHeight / 2 - 100;
			this.addChild(m_menuContainer);
			
			m_menuBase = new Resources.PauseMenuBaseImage() as Bitmap;
			m_menuContainer.addChild(m_menuBase);
			
			var textFormat:TextFormat = new TextFormat("RememberFont", 40);
			textFormat.align = TextFormatAlign.CENTER;
			
			m_pauseLabel = new TextField()
			m_pauseLabel.selectable = false;
			m_pauseLabel.defaultTextFormat = textFormat;
			m_pauseLabel.width = this.stage.stageWidth;
			m_pauseLabel.embedFonts = true;
			m_pauseLabel.height = 60;
			m_pauseLabel.text = "Pause";
			m_pauseLabel.x = m_menuContainer.width / 2 - m_pauseLabel.width / 2;
			m_pauseLabel.y = 10;
			m_menuContainer.addChild(m_pauseLabel);
			
			m_continueButton = new CButton(Resources.ButtonBaseImage, onContinueButtonClick, "Continue", 20);
			m_continueButton.x = m_menuBase.width / 2;
			m_continueButton.y = 90;
			m_menuContainer.addChild(m_continueButton);
			
			m_menuButton = new CButton(Resources.ButtonBaseImage, onMenuButtonClick, "Menu", 20);
			m_menuButton.x = m_menuBase.width / 2;
			m_menuButton.y = 150;
			m_menuContainer.addChild(m_menuButton);
		}
		
		private function onContinueButtonClick():void
		{
			StageManager.instance.popStage();
			GameStage.instance.continueGame();
		}
		
		private function onMenuButtonClick():void
		{
			StageManager.instance.popStage();
			StageManager.instance.popStage();
			GameStage.instance.remove();
			StageManager.instance.pushStage(new MenuStage());
		}
		
		private function initBackground(width:Number, height:Number, color:uint):Bitmap
		{
			var bmpData:BitmapData = new BitmapData(width, height, false, color);
			var bitmap:Bitmap = new Bitmap(bmpData);
			return bitmap;
		}
	}
}