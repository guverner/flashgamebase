﻿package  
{
	import flash.utils.Dictionary;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class StageManager 
	{	
		private static var 	m_instance:StageManager;
		private static var 	m_isAllowInstantiation:Boolean;
		
		private var 		m_stages:Array;
		private var 		m_mainGameSprite:Sprite;
		private var 		m_backgrounds:Dictionary;
		
		public function StageManager() 
		{
			if (!m_isAllowInstantiation)
			{
				throw new Error("Error: Instantiation failed: Use StageManager.instance instead of new.");
			}
			
			initialise();
		}		
		
		public static function get instance():StageManager 
		{
			if (!m_instance)
			{
				m_isAllowInstantiation = true;
				m_instance = new StageManager();
				m_isAllowInstantiation = false;
			}
			
			return m_instance;
		}
		
		private function initialise():void
		{
			m_stages = new Array();
			m_mainGameSprite = MainGame.instance;
			m_backgrounds = new Dictionary();
		}
		
		public function pushStage(newStage:CStage):void
		{
			if (m_stages.length != 0)
			{
				CStage(m_stages[m_stages.length - 1]).touchable = false;
			}
			
			createBackground(newStage.stageName);
			
			m_stages.push(newStage);
			m_mainGameSprite.addChild(newStage);
		}
		
		public function popStage():void
		{
			removeBackground(CStage(m_stages[m_stages.length - 1]).stageName);
			
			m_mainGameSprite.removeChild(m_stages[m_stages.length - 1]);
			m_stages.pop();
			
			if (m_stages.length != 0)
			{
				CStage(m_stages[m_stages.length - 1]).touchable = true;
			}
		}
		
		private function removeBackground(stageName:String):void
		{
			m_mainGameSprite.removeChild(m_backgrounds[stageName]);
			delete m_backgrounds[stageName];
		}
		
		private function createBackground(stageName:String):void
		{
			var bmpData:BitmapData = new BitmapData(m_mainGameSprite.stage.stageWidth, m_mainGameSprite.stage.stageHeight, false, 0x00000);
			var bitmap:Bitmap = new Bitmap(bmpData);
			bitmap.alpha = 0.7;
			m_mainGameSprite.addChild(bitmap);
			m_backgrounds[stageName] = bitmap;
		}
		
		public function moveToStage(stageName:String):void
		{
			var isStartToDeleteStages:Boolean = false;
			
			for (var indStage:int = 0; indStage < m_stages.length; indStage++)
			{
				if (!isStartToDeleteStages)
				{
					if (CStage(m_stages[indStage]).stageName == stageName)
					{
						isStartToDeleteStages = true;
						CStage(m_stages[indStage]).touchable = true;
					}
				}
				else
				{
					removeBackground(CStage(m_stages[indStage]).stageName);
					m_mainGameSprite.removeChild(m_stages[indStage]);
					m_stages.splice(indStage, 1);
					indStage--;
				}
			}
		}
		
		public function currentStage():CStage
		{
			return m_stages[m_stages.length - 1];
		}
	}
}