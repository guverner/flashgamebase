﻿package  
{
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class Resources 
	{			
//		[Embed(source="../res/Cat.xml", mimeType = "application/octet-stream")]
//		public static const CatXml:Class;

		[Embed(source="/res/bone.png")]
		public static const BoneImage:Class;
		
		[Embed(source="/res/pauseButton.png")]
		public static const PauseButtonImage:Class;
		
		[Embed(source="/res/pauseMenuBase.png")]
		public static const PauseMenuBaseImage:Class;
		
		[Embed(source="/res/skull.png")]
		public static const SkullImage:Class;
		
		[Embed(source="/res/buttonBase.png")]
		public static const ButtonBaseImage:Class;
		
		[Embed(source="/res/tileChest.png")]
		public static const TileChestImage:Class;
		
		[Embed(source="/res/tileEarth.png")]
		public static const TileEarthImage:Class;
		
		[Embed(source="/res/tileGrass.png")]
		public static const TileGrassImage:Class;
		
		[Embed(source="/res/vaza.png")]
		public static const VazaImage:Class;
		
		[Embed(source="/res/Remember.ttf", fontName="RememberFont", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		public static const RememberFont:Class;
	}
}